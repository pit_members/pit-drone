#!/bin/bash

wget https://github.com/umlaeute/v4l2loopback/archive/v0.12.3.tar.gz
tar -xvf v0.12.3.tar.gz
cd v4l2loopback-0.12.3
make && sudo make install && sudo depmod -a
cd ..