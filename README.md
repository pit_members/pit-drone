# PIT Drone

## Dépendances

Les bibliothèques Python suivantes sont nécessaires pour la gestion du drone :

- `paho-mqtt`
- `dronekit`

Les bibliothèques Python suivantes sont nécessaires pour émuler le drone :

- `dronekit-sitl`
- `mavproxy`

Voir le wiki [Simuler un drone](https://gitlab.com/pit_members/pit-drone/-/wikis/Simuler-un-drone) pour plus d'information.

La bibliothèque v4l2 loopback est nécessaire pour la partie video, l'installation est automatisé via le script `install_loopback_package.sh`.

## Lancer une simulation

Il faut lancer au préalable dans 3 terminaux différent les scripts :
- `simulation/run_drone.sh`
- `simulation/run_mavlink.sh`
- `video_loopback.sh`

Il est alors possible de lancer dans un 4ème terminal le script `drone.py`, auquel il est nécessaire de passer l'id de l'intervention à utiliser.