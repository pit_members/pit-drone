#!/usr/bin/python3

from argparse import ArgumentParser
from base64 import b64encode
from enum import Enum
from json import loads, dumps
from math import sqrt
from os import remove, getcwd
from subprocess import run
from tempfile import NamedTemporaryFile
from threading import Thread, Lock, Event
from time import sleep, gmtime, strftime

from dronekit import connect, VehicleMode, LocationGlobalRelative
from paho.mqtt.client import Client
from pymavlink import mavutil
from requests import post

# Variables propres aux projets
__ip_serveur = "pitprojectm2.ddns.net"
__port_backend = 11001
__port_mqtt = 12001
__drone_connect_string = "udp:127.0.0.1:14551"
__base_topic_mqtt = "pit/sync/events"

# Utilisation du mode debug
debug = False


class DroneWrapper:
    """
    Classe de contrôle d'un drone
    Classe conteneur d'un drone, s'assurant de la création et de l'état du drone à tout instant.
    Elle propose toutes les méthodes utilitaires pour contrôler le drone.
    """

    class State(Enum):
        """
        L'état macro du drone
        """
        GROUND = "GROUND"
        MOVING = "MOVING"
        IDLE = "IDLE"

    class __MovingThread(Thread):
        """
        Une classe interne pour le suivi du drone en mouvement, interruptible.

        Une classe interne pour le suivi du drone en mouvement, démarré par le wrapper à
        chaque appel à une méthode de déplacement (arm, rtl, goto). Elle est nécessaire
        car chaque méthode de déplacement ne permet pas de déterminer sa complétion.
        Il est donc nécessaire de vérifier si le déplacement est bien fini.
        """

        def __init__(self, drone_wrapper, next_state, target, lock):
            """
            Constructeur.

            :param drone_wrapper: DroneWrapper la classe appelante, contenant le drone
            :param next_state: DroneWrapper.State le state qu'aura le drone une fois la target atteinte
            :param target: dronekit.LocationGlobalRelative le prochain emplacement souhaité du drone
            :param lock: le verrou de synchronisation du thread, pour la modification interne de la classe appelante
            """
            super().__init__()
            self.__drone_wrapper = drone_wrapper
            self.__target = target
            self.__next_state = next_state
            self.interrupt = Event()
            self.__lock = lock

        def __get_max_velocity(self):
            """
            Retourne la plus grande vélocité absolue actuelle
            :return: number [ > 0 ]
            """
            velocity = self.__drone_wrapper.get_velocity()
            return max([abs(velocity[0]), abs(velocity[1]), abs(velocity[2])])

        def __get_distance_metres(self):
            """
            Renvoie la distance en mètres restantes à parcourir
            :return: number [ > 0 ]
            """
            location1 = self.__drone_wrapper._DroneWrapper__get_current_location()
            location2 = self.__target
            dlat = location2.lat - location1.lat
            dlong = location2.lon - location1.lon
            dalt = location2.alt - location1.alt
            distance = sqrt((dlat * dlat) + (dlong * dlong)) * 1.113195e5
            return sqrt(distance * distance + dalt * dalt)

        def run(self):
            """
            Lance le thread de suivi du drone.
            """
            if debug:
                print("Start MovingThread.")

            count = 0
            while not self.interrupt.is_set():
                if debug:
                    print("MovingThread remaining distance is ", self.__get_distance_metres(), " meters.")

                drone_reach_point: bool = self.__get_distance_metres() < 1 or \
                                          (self.__target.alt > 1 and count >= 10 and self.__get_max_velocity() < 0.1)
                if drone_reach_point:
                    if self.__lock.acquire(blocking=False):
                        if self.interrupt.is_set():
                            self.__lock.release()
                            break
                        self.__drone_wrapper._DroneWrapper__state = self.__next_state
                        self.__drone_wrapper._DroneWrapper__target_thread = None
                        self.__drone_wrapper._DroneWrapper__target_thread_lock.release()
                        if debug:
                            print("MovingThread finished.")

                        return
                    if debug:
                        print("MovingThread failed to acquire lock.")

                else:
                    if debug:
                        print("MovingThread sleep for 1 seconds.")

                    sleep(1)
                count += 1
            if debug:
                print("MovingThread interrupted.")

    def __init__(self, connect_string):
        """
        constructeur
        :param connect_string: str la chaine de connexion utilisé pour se connecter au drone
        """
        self.__drone = connect(connect_string, wait_ready=True)
        self.__state = DroneWrapper.State.GROUND
        self.__home = self.__get_current_location()

        self.__target_thread_lock = Lock()
        self.__target_thread = None

        # 22 m/s ~ 80 km/h. Il s'agit du maximum que le drone peut atteindre.
        # La vitesse réelle dépendra de la longueur du déplacement.
        self.__drone.groundspeed = 22
        self.__drone.airspeed = 22

    def goto(self, latitude, longitude):
        """
        Démarre un déplacement du drone vers une nouvelle position, et lance un thread de suivi.
        PREREQUIS : il est nécessaire d'avoir fait décoller (ARM) le drone avant de faire un goto.
        :param latitude: latitude de la nouvelle position
        :param longitude: longitude de la nouvelle position
        """
        if debug:
            print("DroneWrapper goto call with latitude=", latitude, "and longitude=", longitude)

        target = LocationGlobalRelative(latitude, longitude, self.__get_current_location().alt)
        self.__drone.wait_for_mode(VehicleMode("GUIDED"))
        self.__drone.simple_goto(target)
        self.__condition_yaw(0)
        self.__state = DroneWrapper.State.MOVING
        self.__start_moving_thread(target, DroneWrapper.State.IDLE)

    def arm(self, altitude):
        """
        Fait décoller le drone et lance un thread de suivi.
        :param altitude: float L'altitude à laquelle se stabiliser après décollage
        """
        if debug:
            print("DroneWrapper arm call with altitude=", altitude)

        self.__drone.wait_for_armable()
        self.__drone.wait_for_mode(VehicleMode("GUIDED"))
        self.__drone.arm()
        self.__drone.simple_takeoff(altitude)
        self.__state = DroneWrapper.State.MOVING
        self.__start_moving_thread(LocationGlobalRelative(self.__home.lat, self.__home.lon, altitude),
                                   DroneWrapper.State.IDLE)

    def rtl(self):
        """
        Fait rentrer le drone à la base et lance un thread de suivi. Pour éviter tout problème technique,
        il est conseillé de ne pas interrompre un RTL (et donc d'attendre que l'état du drone passe à GROUD)
        et de faire appel à disarm_and_statibilize pour assurer une utilisation prochaine dans des conditions optimales.
        """
        if debug:
            print("DroneWrapper rtl call")

        self.__drone.wait_for_mode(VehicleMode("RTL"))
        self.__state = DroneWrapper.State.MOVING
        self.__start_moving_thread(self.__home, DroneWrapper.State.GROUND)

    def get_position(self):
        """
        Renvoie la position actuelle du drone
        :return: (latitude, longitude, altitude)
        """
        current = self.__get_current_location()
        return (current.lat, current.lon, current.alt)

    def get_state(self):
        """
        Renvoie l'état actuel du drone
        :return: DroneWrapper.State
        """
        return self.__state

    def get_batterie_level(self):
        """
        Renvoie le niveau de batterie actuelle du drone
        :return: number
        """
        return self.__drone.battery.level

    def get_velocity(self):
        """
        Renvoie la vélocité actuelle du drone, sous forme de vecteurs d'accélérations orthonormés
        :return: (Vx, Vy, Vz)
        """
        return self.__drone.velocity

    def disarm_and_stabilize(self):
        """
        Désarme le drone et le stabilise, à terre. À appeler après un RTL, pour assurer un redécollage optimal.
        """
        if debug:
            print("DroneWrapper disarm call")

        self.__drone.disarm()
        self.__drone.wait_for_mode(VehicleMode("STABILIZE"))

    def __condition_yaw(self, heading, relative=False):
        """
        Détermine la direction à laquelle le drone doit faire face.
        Par exemple, pour que celui-çi soit orienté au Nord, il suffit d'appeler cette méthode avec heading = 0.
        Cette méthode peut être appelée en parallèle d'une autre méthode de déplacement.
        :param heading: float [0;360[] coordonées sur une boussole (0 = Nord, 90 = Est, 180 = Sud, 270 = Ouest)
        :param relative: bool Indique si le lacet est relatif à l'orientation du drone ou s'il s'agit d'un angle absolu
        """
        if relative:
            is_relative = 1  # yaw relative to direction of travel
        else:
            is_relative = 0  # yaw is an absolute angle
        # create the CONDITION_YAW command using command_long_encode()
        msg = self.__drone.message_factory.command_long_encode(
            0, 0,  # target system, target component
            mavutil.mavlink.MAV_CMD_CONDITION_YAW,  # command
            0,  # confirmation
            heading,  # param 1, yaw in degrees
            0,  # param 2, yaw speed deg/s
            1,  # param 3, direction -1 ccw, 1 cw
            is_relative,  # param 4, relative offset 1, absolute angle 0
            0, 0, 0)  # param 5 ~ 7 not used
        # send command to vehicle
        self.__drone.send_mavlink(msg)

    def __get_current_location(self):
        """
        Renvoie la position actuelle du drone.
        Bloquante jusqu'à obtenir une position non-None.
        :return dronekit.LocationGlobalRelative
        """
        current = self.__drone.location.global_relative_frame
        while current.lat is None or current.lon is None or current.alt is None:
            sleep(0.2)
            current = self.__drone.location.global_relative_frame
        return current

    def __start_moving_thread(self, target, next_state):
        """
        Lance le thread de suivi du drone.
        Si un thread existe déjà, l'interrompt et lance un nouveau, qui aura pour objectif la cible fournie
        et passera l'état du drone à next_state une fois cet emplacement atteint.
        :param target: dronekit.LocationGlobalRelative prochaine position souhaitée du drone
        :param next_state: DroneWrapper.State le prochain état du drone, une fois la target atteinte
        """
        self.__stop_moving_thread()
        self.__target_thread = self.__MovingThread(self, next_state, target, self.__target_thread_lock)
        self.__target_thread.start()

    def __stop_moving_thread(self):
        """
        Interrompt un éventuel thread de suivi de mouvement.
        """
        with self.__target_thread_lock:
            if self.__target_thread:
                self.__target_thread.interrupt.set()
                self.__target_thread = None

    def __del__(self):
        """
        Destructeur
        """
        self.__stop_moving_thread()
        self.__drone.close()


class ControllerMQTT(Thread):
    """
    Contrôleur principal de l'application.

    Permet, en lui passant les composants nécessaires, de contrôler le drone ainsi que la caméra
    à partir de messages MQTT.
    """

    class __LoggerDrone(Thread):

        def __init__(self, client, topic, drone):
            """
            Constructeur
            :param client: paho.mqtt.client.Client client MQTT
            :param topic: list[str] la liste des topics sur lequel le client MQTT publie
            :param drone:
            """
            super().__init__()
            self.__client = client
            self.__drone = drone
            self.__topic = topic
            self.interrupt = Event()

        def run(self):
            """
            Lance le thread de log du drone
            """
            if debug:
                print("LoggerDrone started.")

            while True:
                message = {
                    'eventType': 'DRONE_STATE',
                    'eventTime': get_time(),
                    'eventNo': 0,
                    'data': dict()
                }
                message['data']['latitude'], message['data']['longitude'], message['data'][
                    'altitude'] = self.__drone.get_position()
                message['data']['state'] = self.__drone.get_state().name
                if debug:
                    print("LoggerDrone : ", message)

                for topic in self.__topic:
                    self.__client.publish(topic, dumps(message), 2)
                if self.interrupt.wait(1):
                    break
            if debug:
                print("LoggerDrone interrupted.")

    class __CallbackThread(Thread):
        """
        Un thread executant un callback lors de la statisfaction de la condition. Interruptible.
        """

        def __init__(self, controller, drone, wait_state, callback, lock):
            """
            Constructeur
            :param controller: ControllerMQTT le controlleur appelant
            :param drone: DroneWrapper l'interface du drone
            :param wait_state: DroneWrapper.State l'état du drône à atteindre avant d'appeler le callback
            :param callback: la fonction callback à rappeler
            :param lock: threading.Lock le verrou de synchronisation avec le contrôleur appelant
            """
            super().__init__()
            self.interrupt = Event()
            self.__drone = drone
            self.__wait_state = wait_state
            self.__callback = callback
            self.__lock = lock
            self.__controller = controller

        def run(self):
            """
            Thread.run
            Lance le thread de vérification du prochain état atteint, pour appeler le callback.
            """
            if debug:
                print("CallbackThread started.")
            while not self.interrupt.wait(1):
                if self.__drone.get_state() is self.__wait_state:
                    if self.__lock.acquire(blocking=False):
                        if self.interrupt.is_set():
                            self.__lock.release()
                            break
                        self.__controller._ControllerMQTT__callback_thread = None
                        self.__lock.release()
                        if debug:
                            print("CallbackThread finished.")
                        return self.__callback()
                    if debug:
                        print("CallbackThread failed to get lock.")
            if debug:
                print("CallbackThread interrupted.")

    class __BatterieChecker(Thread):
        """
        Vérifie l'état de la batterie, appelle la méthode d'arrêt d'urgence si la batterie du drone est trop faible.
        Interruptible.
        """

        def __init__(self, controller, drone, level):
            """
            Constructeur
            :param controller: ControllerMQTT le controller appelant
            :param drone: DroneWrapper l'interface du drone
            :param level: float le niveau critique de batterie du drone
            """
            super().__init__()
            self.__controller = controller
            self.__drone = drone
            self.__level = level
            self.interrupt = Event()

        def run(self):
            """
            Lance le thread.
            Lance le thread de vérification du niveau de batterie du drone.
            """
            if debug:
                print("BatterieChecker started.")
            while not self.interrupt.wait(1):
                if self.__drone.get_batterie_level() < self.__level:
                    self.__controller._ControllerMQTT__emergency_rtl()
                    if debug:
                        print("BatterieChecker finished.")
                    return
            if debug:
                print("BatterieChecker interrupted.")

    def __init__(self, drone, media_manager, host, port, topic_log, topic_listen, topic_publish):
        """
        Constructeur
        :param drone: DroneWrapper L'interface du drone
        :param media_manager: MediaManager L'interface de gestion des medias (photos/videos)
        :param host: str L'adresse du broker MQTT
        :param port: int Le port du broker MQTT
        :param topic_log: str Le topic utilisé pour logger l'état du drone
        :param topic_listen: str Le topic utilisé pour écouter les commandes du serveur distant
        :param topic_publish: str Le topic utilisé pour publier les acquittements des succès de commandes au
        serveur distant
        """
        super().__init__()
        self.__drone = drone
        self.__media_manager = media_manager
        self.__mqtt = Client()
        self.__topic_log = topic_log
        self.__topic_listen = topic_listen
        self.__topic_publish = topic_publish
        self.__host = host
        self.__port = port

        self.__logging_thread = self.__LoggerDrone(self.__mqtt, [topic_log, topic_publish], drone)
        self.__callback_thread = None
        self.__callback_thread_lock = Lock()
        self.__batterie_checker_thread = self.__BatterieChecker(self, drone, 10)

        # Variable interne pour indiquer la localisation d'une photo
        self.__next_latitude = None
        self.__next_longitude = None

        # Permet de faire un switch automatique suivant le type de message reçu
        self.__message_switcher = {
            'NEXT_POINT': self.__on_next_point,
            'RTL': self.__on_rtl,
            'ARM': self.__on_arm,
            'LOGOUT': self.__on_logout
        }

    def run(self):
        """
        Lance le contrôleur MQTT, ainsi que les threads associés. Non interruptible.
        S'arrête lors de l'appel à la méthode stop.
        """
        self.__mqtt.on_connect = self.__on_connect_client_mqtt()
        self.__mqtt.on_message = self.__on_message_client_mqtt()
        self.__mqtt.connect(self.__host, self.__port, 60)
        self.__logging_thread.start()
        self.__media_manager.start_uploading_video()
        self.__mqtt.loop_forever()

    def __on_connect_client_mqtt(self):
        """
        Définit la méthode appelée lors de la connexion avec succès du client MQTT au broker distant.
        """

        def on_connect(client, userdata, flags, rc):
            print("Client is listening")
            client.subscribe(self.__topic_listen, 2)

        return on_connect

    def __on_message_client_mqtt(self):
        """
        Définit la méthode appelée lors de la réception d'un message MQTT.
        :return:
        """

        def on_message(client, userdata, msg):
            message = loads(msg.payload)
            print("Receive : ", message)
            self.__message_switcher.get(message['eventType'], self.__on_unknown)(message)

        return on_message

    def __on_next_point(self, message):
        """
        Méthode appelée lors de la réception d'un message MQTT de type NEXT_POINT.
        Lance le déplacement du drone au point indiqué, ainsi que le thread de callback pour son arrivée au point.
        """
        latitude = message['data']['latitude']
        longitude = message['data']['longitude']
        self.__drone.goto(latitude, longitude)
        self.__next_latitude = latitude
        self.__next_longitude = longitude
        self.__start_callback_thread(DroneWrapper.State.IDLE, self.__callback_on_next_point)

    def __callback_on_next_point(self):
        """
        Callback lors de l'arrivée du drone au point voulu, prise d'une photo et envoie du message d'acquittement
        ARRIVED_TO_POINT au broker de messages.
        """
        sleep(1)
        self.__media_manager.take_and_upload_photo(self.__next_latitude, self.__next_longitude)
        sleep(1)
        message = {
            'eventType': 'ARRIVE_TO_POINT',
            'eventTime': get_time(),
            'eventNo': 0
        }
        self.__secure_publish(dumps(message))

    def __on_rtl(self, message):
        """
        Méthode appelée lors de la réception d'un message MQTT de type RTL.
        Lance le retour à la base du drone, ainsi que le thread de callback pour son arrivée à la base.
        """
        self.__drone.rtl()
        if self.__batterie_checker_thread.is_alive():
            self.__batterie_checker_thread.interrupt.set()
            self.__batterie_checker_thread = self.__BatterieChecker(self, drone, 10)
        self.__start_callback_thread(DroneWrapper.State.GROUND, self.__callback_on_rtl)

    def __callback_on_rtl(self):
        """
        Callback de la méthode ControllerMQTT.__on_rtl.
        Callback lors de l'arrivée du drone à la base, envoie du message d'acquittement RTL_DONE au broker.
        """
        self.__drone.disarm_and_stabilize()
        message = {
            'eventType': 'RTL_DONE',
            'eventTime': get_time(),
            'eventNo': 0
        }
        self.__secure_publish(dumps(message))

    def __on_arm(self, message):
        """
        Méthode appelée lors de la réception d'un message MQTT de type ARM
        Lance le décollage du drone, ainsi que le thread de callback pour son arrivée à son altitude souhaitée
        """
        self.__drone.arm(20)
        if not self.__batterie_checker_thread.is_alive():
            self.__batterie_checker_thread.start()
        self.__start_callback_thread(DroneWrapper.State.IDLE, self.__callback_on_arm)

    def __callback_on_arm(self):
        """
        Callback de la méthode ControllerMQTT.__on_arm
        Callback lors de l'arrivée du drone à son altitude de mission, envoie du message d'acquittement ARM_DONE au broker.
        """
        message = {
            'eventType': 'ARM_DONE',
            'eventTime': get_time(),
            'eventNo': 0
        }
        self.__secure_publish(dumps(message))

    def __on_logout(self, message):
        """
        Méthode appelée lors de la réception d'un message MQTT de type LOGOUT
        Appel la méthode ControllerMQTT.__stop
        """
        self.__stop()

    def __on_unknown(self, message):
        """
        Méthode appelée lors de la réception d'un message inconnu du broker.
        """
        print("Unknow message : {}".format(message))

    def __stop(self):
        """
        Termine la mission.
        Arrête les threads de suivis, fait rentrer le drone à la base et arrête le client MQTT.
        Termine l'exécution de ControllerMQTT.run()
        """
        if self.__batterie_checker_thread and self.__batterie_checker_thread.is_alive():
            self.__batterie_checker_thread.interrupt.set()
        self.__stop_callback_thread()
        self.__media_manager.stop_uploading_video()
        self.__logging_thread.interrupt.set()
        if self.__drone.get_state() is not DroneWrapper.State.GROUND:
            self.__drone.rtl()
        while self.__drone.get_state() is not DroneWrapper.State.GROUND:
            sleep(1)
        self.__mqtt.disconnect()

    def __emergency_rtl(self):
        """
        Méthode appelée par le thread de surveillance de la batterie lorsque la batterie est trop faible.
        Appelle la méthode ControllerMQTT.__stop() et envoie un message au broker MQTT sur tous les topics de
        publications.
        """
        print("BATTERY TOO LOW : EMERGENCY_RTL")
        message = {
            'eventType': 'EMERGENCY_RTL',
            'eventTime': get_time(),
            'eventNo': 0
        }
        self.__secure_publish(dumps(message))
        self.__mqtt.publish(self.__topic_log, dumps(message), 2)
        self.__stop()

    def __stop_callback_thread(self):
        """
        Arrête et supprime le thread de callback.
        """
        with self.__callback_thread_lock:
            if self.__callback_thread:
                self.__callback_thread.interrupt.set()
                self.__callback_thread = None

    def __start_callback_thread(self, wait_state, callback):
        """
        Démarre un nouveau thread de callback.
        Si besoin, arrête un thread de callback existant
        """
        self.__stop_callback_thread()
        self.__callback_thread = self.__CallbackThread(self, self.__drone, wait_state, callback,
                                                       self.__callback_thread_lock)
        self.__callback_thread.start()

    def __secure_publish(self, message):
        """
        Publie et assure la publication d'un message sur le broker sur le topic ControllerMQTT.__topic_publish
        """
        print("Send : ", message)
        self.__mqtt.publish(self.__topic_publish, message, 2).wait_for_publish()


class MediaManager:
    """
    Clase de gestion des contenus photo et videos
    """

    class __VideoUploader(Thread):
        """
        Classe de prise et d'upload de la video. Interruptible.
        La vidéo consiste en une série de photos prise à une fréquence minimum de 60 images par secondes
        et qui est uploadée à chaque prise.
        """

        def __init__(self, media_manager):
            """
            Constructeur
            :param media_manager: MediaManager Classe appelante
            """
            super().__init__()
            self.__media_manager = media_manager
            self.interrupt = Event()

        def run(self):
            """
            Lance le thread de la prise vidéo
            """
            if debug:
                print("VideoUploader started")

            while not self.interrupt.wait(1 / 60):
                frame = self.__media_manager._MediaManager__take_photo(
                    self.__media_manager._MediaManager__generate_unique_name()
                )
                post(
                    url="http://{}:{}/interventions/{}/drone/video".format(
                        self.__media_manager._MediaManager__host,
                        self.__media_manager._MediaManager__port,
                        self.__media_manager._MediaManager__intervention_id
                    ),
                    data=frame
                )
                if debug:
                    print("VideoUploader frame send.")
            if debug:
                print("VideoUploader interrupted.")

    def __init__(self, host, port, intervention_id):
        """
        Constructeur
        :param host: str Adresse du serveur distant
        :param port: int Port du serveur distant
        :param intervention_id: int Identifiant de l'intervention en cours
        """
        self.__host = host
        self.__port = port
        self.__intervention_id = intervention_id
        self.__video_uploader_thread = None

    def take_and_upload_photo(self, latitude, longitude):
        """
        Prend et transmet au serveur une photo
        :param latitude: float la latitude du lieu où a été prise la photo
        :param longitude: float la longitude du lieu où a été prise la photo
        """
        photo = self.__take_photo(self.__generate_unique_name())
        post(
            url="http://{}:{}/interventions/{}/drone/photo?latitude={}&longitude={}".format(
                self.__host,
                self.__port,
                self.__intervention_id,
                latitude,
                longitude),
            data=photo)
        if debug:
            print("MediaManager take and upload photo call successfully with latitude=", latitude,
                  " and longitude=", longitude)

    def __take_photo(self, name):
        """
        Prend une photo et la renvoie en un String en Base64
        :return str photo codée en Base64
        """
        command = "ffmpeg -f v4l2 -i /dev/video10 -f image2 -frames:v 1 {} 1>/dev/null 2>/dev/null".format(name)
        run(command, shell=True)
        f = open(name, "rb")
        data = f.read()
        f.close()
        remove(name)
        return b64encode(data)

    def __generate_unique_name(self):
        """
        Génère un nom unique
        """
        ntf = NamedTemporaryFile(delete=True, dir=getcwd())
        name = ntf.name
        ntf.close()
        return name

    def start_uploading_video(self):
        """
        Commence à filmer et uploader la vidéo sur le serveur distant
        Lance un thread __VideoUploader s'il n'existe pas déjà
        """
        if self.__video_uploader_thread and not self.__video_uploader_thread.interrupt.is_set:
            return
        self.__video_uploader_thread = self.__VideoUploader(self)
        self.__video_uploader_thread.start()

    def stop_uploading_video(self):
        """
        Arrête la prise vidéo
        Stoppe le thread __VideoUploader s'il existe
        """
        self.__video_uploader_thread.interrupt.set()
        self.__video_uploader_thread = None


def get_time():
    """
    Renvoie la date du jour (à la précision d'une seconde) sous format String
    :return str
    """
    return strftime("%Y-%m-%dT%H:%M:%S", gmtime())


def _parser():
    """
    Récupère les arguments de ligne de commande
    :return un dictionnaire des valeurs récupéré
    """
    parser = ArgumentParser(
        description=""  # TODO : Fill the description
    )
    parser.add_argument(
        "intervention_id",
        type=int,
        help=""  # TODO
    )
    parser.add_argument(
        "--debug",
        action="store_true"
    )
    return parser.parse_args()


def register_to_intervention(url_serveur, intervention_id):
    """
    Contacte le serveur distant pour enregister le drone dans l'intervention.
    :param url_serveur: str L'Url du serveur
    :param intervention_id: int L'id de l'intervention
    """
    response = post(url="{}/interventions/{}/drone/register".format(url_serveur, intervention_id))
    if int(response.status_code / 100) != 2:
        raise RuntimeError("Can't register to the given interventionID")


if __name__ == "__main__":
    args = _parser()
    debug = args.debug

    topic_log = "{}/{}".format(__base_topic_mqtt, args.intervention_id)
    topic_publish = "{}/drone".format(topic_log)
    topic_listen = "{}/commands".format(topic_publish)

    drone = DroneWrapper(__drone_connect_string)
    register_to_intervention("http://{}:{}".format(__ip_serveur, __port_backend), args.intervention_id)
    media_manager = MediaManager(__ip_serveur, __port_backend, args.intervention_id)
    controller = ControllerMQTT(drone, media_manager, __ip_serveur, __port_mqtt, topic_log, topic_listen, topic_publish)
    controller.run()
