#!/bin/bash

# create the video stream device
sudo modprobe v4l2loopback video_nr=10

# stream the video to the video stream device
ffmpeg -f v4l2 -i /dev/video0 -codec copy -f v4l2 /dev/video10

# delete the video stream device
sudo modprobe -r v4l2loopback